// Invoke 'strict' JavaScript mode
'use strict';

// Load the 'users' controller
var users = require('../../app/controllers/users.server.controller'),
	passport = require('passport');

// Define the routes module' method
module.exports = function(app) {
	app.route('/signup')
        .get(users.renderSignup)
        .post(users.signup);

	app.route('/signin')
        .get(users.renderSignIn)
        .post(passport.authenticate('local',{
            successRedirect : '/',
            failureRedirect : '/signin',
            failureFlash : true
        }));

	app.get('/signout',users.signout);

	// Set up the 'users' base routes 
	app.route('/users')
	   .post(users.create)
	   .get(users.list);

	// Set up the 'users' parameterized routes
	app.route('/users/:userId')
	   .get(users.read)
	   .put(users.update)
	   .delete(users.delete);
	//facebook login strategy
    app.get('/oauth/facebook',passport.authenticate('facebook',{
        failureRedirect :'/signin'
    }));
    app.get('/oauth/facebook/callback',passport.authenticate('facebook',{
        failureRedirect : '/signin',
        successRedirect: '/'
    }))

	// Set up the 'userId' parameter middleware
	app.param('userId', users.userByID);
};