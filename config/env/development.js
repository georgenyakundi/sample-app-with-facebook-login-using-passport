// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'development' environment configuration object
module.exports = {
	db: 'mongodb://localhost:27017/mean-development',
	sessionSecret: 'developmentSessionSecret',

	facebook:{
		clientID:'Add your client id here',
        clientSecret:'Add your client secret here',
        callbackURL:'http://localhost:3000/oauth/facebook/callback'
	}
};